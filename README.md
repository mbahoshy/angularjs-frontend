#AngularJS Front-end Test


## Objective

Use AngularJS to create a simple front end client connected to a JSON API. This repository contains a Node.js app with a completed API. You will add the AngularJS front end in the *public* directory

## Running the project

The project requires Node.js and a local running instance of MongoDB. For convenience the NPM modules have been checked into the repository. After cloning the repo, start the project by running `npm start`. If you need any help running the project contact sean@smartsrc.com

## The API

The API provides access to a local database that stores contacts. It has the following endpoints:

### GET: /api/contacts

Returns an array of contact resources

```
[
  {
    "_id":"53946910bc23ff130368d92e",
    "firstName":"Sean",
    "lastName":"Cain",
    "email":"sean@smartsrc.com",
    "company":"smartsource"
  }
]
```

### GET: /api/contacts/{_id}

Returns a single contact

```
{
  "_id":"53946910bc23ff130368d92e",
  "firstName":"Sean",
  "lastName":"Cain",
  "email":"sean@smartsrc.com",
  "company":"smartsource"
}
```

### POST: /api/contacts

Creates a new contact. The response will contain the new contact's id.


#### Request Body

```
{
  "firstName":"Jane",
  "lastName":"Done",
  "email":"jane@acme.com",
  "company":"ACME"
}
```

#### Response Body


```
{
  "_id":"53946910bc23ff130368d92e",
  "firstName":"Jane",
  "lastName":"Doe",
  "email":"jane@gmail.com"
}
```

### PATCH: /api/contacts/{id}


Updates an existing contact. The request will send properties to add/change. The response will return the resulting state of the contact.


#### Request Body

```
{
  "email":"jane@acme.com",
  "company":"ACME"
}
```

#### Response Body


```
{
  "_id":"53946910bc23ff130368d92e",
  "firstName":"Jane",
  "lastName":"Doe",
  "email":"jane@acme.com"
  "company": "ACME"
}
```

### DELETE: /api/contacts/{id}

Delete's a contact and returns an empty response

## Your Task

Create a CRUD application that allows a user to view all contacts, create new contacts, edit an existing contact and delete an existing contact. The application should only create valid contacts. Contacts require a first name, last name and valid email address.

## Submitting your solution

When you're finished submit a pull request to this repository.