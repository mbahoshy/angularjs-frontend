// controllers.js

cm.controller("HomeController", function ($scope, $http, $location) {

	var lpp = 5, //listings per page
		contactHolder = [],
		pages = [];

	$http.get('/api/contacts').success(function(data, status) {
		var numContacts = data.length,
			numPages = (numContacts / lpp);

		for(var i =0; i < numPages; i++) {
			var x = data.splice(0, lpp);
			contactHolder.push(x);
			pages[i] = {pageid: i, pagenum: (i +1)}; 
		}

		$scope.contacts = contactHolder[0]; //generates page contacts
		$scope.pages = pages; //generates pagination
		
	}).error(function(data, status) {
		alert('An error occurred');
		$location.url('/');
		console.dir(status);
	});

	$scope.pageClick = function (page, e) {
		$scope.contacts = contactHolder[page];

		$('.active-page').removeClass('active-page');
		$(e.target).addClass('active-page');

	}

	$scope.contactClick = function(contact) {
		console.dir(contact);

		$location.url('/contact/' + contact._id);
	}
});

cm.controller("AddContactController", function ($scope, $http, $location) {

	$scope.submitForm = function (e) {
		var form = $scope.submit,
			contact = {
				firstName: form.firstName,
				lastName: form.lastName,
				email: form.email,
				company: form.company
			};

		console.log('Form submitted');
		$http.post('/api/contacts', contact).success(function(data, status) {
			console.dir(data);
			if(data.length > 0) {
				console.log('Database update successful!');
				$location.url('/contact/' + data[0]._id);
				alert('Database update successful!')
			} else {
				console.log('Database failed to update.');
				$location.url('/')
				alert('Database Failed to update.')
			}


		}).error(function(data, status) {
			alert('An error occurred');
			$location.url('/');
			console.dir(status);

		});


	}

});

cm.controller("contactController", function ($scope, $http, $routeParams, $location) {

	var id = $routeParams.id;

	$http.get('/api/contacts/' + id).success(function(data, status) {
		$scope.contact = data;
	}).error(function(data, status) {
		alert('An error occurred');
		$location.url('/');
		console.dir(status);
	});

	$scope.deleteContact = function () {
		if(confirm('Permanently delete this contact?')) {
			$http.delete('/api/contacts/' + id).success(function(data, status){
				console.log('Contact removed from database');
				$location.url('/');
			});
		}
	}


});

cm.controller("editContactController", function ($scope, $http, $routeParams, $location) {

	var contact,
		id = $routeParams.id;


	$http.get('/api/contacts/' + id).success(function(data, status) {
		$scope.contact = data;
		contact = jQuery.extend({}, data); //create a copy of contact object
	}).error(function(data, status) {
		alert('An error occurred');
		$location.url('/');
		console.dir(status);

	});

	$scope.updateContact = function() {

		var changes = {};

		//checks for changes on contact data. Could be condensed as loop
		if ($scope.contact.firstName !== contact.firstName) {
			changes.firstName = $scope.contact.firstName;
		}
		if ($scope.contact.lastName !== contact.lastName) {
			changes.lastName = $scope.contact.lastName;
		}
		if ($scope.contact.email !== contact.email) {
			changes.email = $scope.contact.email;
		}
		if ($scope.contact.company !== contact.company) {
			changes.company = $scope.contact.company;
		}

		$http({
			url: "/api/contacts/" + id,
    		data: changes,
    		method: "PATCH"
    	}).success(function(data, status) {
    		alert('Update successful');
			$location.url('/contact/' + id);
    		
		}).error(function(data, status) {
			alert('Update failed');
    		$location.url('/');
			console.dir(status);
		});
	}

});