// routes.js

cm.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
	$routeProvider.when('/', {
		templateUrl: '../views/home-view.html',
		controller: 'HomeController'
	});
	$routeProvider.when('/add-contact', {
		templateUrl: '../views/add-contact-view.html',
		controller: 'AddContactController'
	});
	$routeProvider.when('/contact/:id', {
		templateUrl: '../views/contact-view.html',
		controller: 'contactController'
	});
	$routeProvider.when('/edit-contact/:id', {
		templateUrl: '../views/edit-contact-view.html',
		controller: 'editContactController'
	});
}]);