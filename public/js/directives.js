//directives.js

cm.directive('header', function () {
	return {
		restrict:'A',
		replace: true,
		templateUrl: '/views/header-view.html'
	};
});