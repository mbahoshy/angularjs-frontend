
var util = require('util')

exports.sendError = function (err, res) {

  var statusCode = err.validationErrors && err.validationErrors.length ? 422 : 500

  var errorBody = {
    message: err.message,
    stack: err.stack,
    validationErrors: err.validationErrors
  }

  res.json(statusCode, errorBody)

}

exports.sendNotFoundError = function (resourceName, id, res) {

  var errorBody = {
    message: util.format('%s %s not found', resourceName, id)
  }

  res.json(404, errorBody)
}
