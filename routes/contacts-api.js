var express = require('express');
var router = express.Router();
var apiErrors = require('./api-errors');
var db = require('../lib/model').db;
var collection = db.collection('contacts');

router.get('/', function(req, res, nex) {

  collection.findItems({}, function(err, contacts) {

    if (err) {
      apiErrors.sendError(err, res)
      return
    }

    res.json(contacts);
  });

});

router.get('/:_id', function(req, res) {

  var _id = req.params._id;

  collection.findById(_id, function(err, contact) {

    if (err) {
      apiErrors.sendError(err, res)
      return
    }


    if (!contact) {
      apiErrors.sendNotFoundError("Contact", req.params._id, res)
      return
    }


    res.json(contact);
  });

});

router.post('/', function(req, res) {

  var contact = req.body;

  collection.insert(contact, function(err, saved) {

    if (err) {
      apiErrors.sendError(err, res)
      return
    }

    res.json(saved);
  })

})

router.patch('/:_id', function(req, res) {

  var _id = req.params._id;
  var changes = req.body;

  collection.updateById(_id, {$set: changes}, function(err, result) {

    if (err) {
      apiErrors.sendError(err, res)
      return
    }

    if (!result) {
      apiErrors.sendNotFoundError("Contact", req.params._id, res)
      return
    }

    collection.findById(_id, function(err, contact) {
      res.json(contact);
    })
  })
})


router.delete('/:_id', function(req, res) {

  var id = req.params._id;
  collection.removeById(id, function(err, result) {

    if (err) {
      apiErrors.sendError(err, res)
      return
    }

    if (!result) {
      apiErrors.sendNotFoundError("Contact", req.params._id, res)
      return
    }

    res.json({})
  })

})


module.exports = router;
