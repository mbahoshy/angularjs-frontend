
var should = require('should')

var db = require('../lib/model').db

describe('Talking to the db', function() {

  it('should make a connection', function(done) {
    db.collection('contacts').findOne({}, function(err, item) {
      should.not.exist(err)
      done()
    })
  })

})
